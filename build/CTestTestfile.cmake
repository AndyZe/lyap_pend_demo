# CMake generated Testfile for 
# Source directory: /home/andy/Desktop/catkin_ws/src
# Build directory: /home/andy/Desktop/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(pendulum_joint_pub)
SUBDIRS(gazebo_ros_demos/rrbot_control)
SUBDIRS(gazebo_ros_demos/rrbot_description)
SUBDIRS(gazebo_ros_demos/rrbot_gazebo)
SUBDIRS(lyap_control)
SUBDIRS(lyap_pendulum)
SUBDIRS(gazebo_ros_demos/custom_plugin_tutorial)
