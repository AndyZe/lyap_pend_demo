#ifndef MODEL_DEFINITION_H
#define MODEL_DEFINITION_H

// The state space definition-- the dynamic equations of the model.
// Calculates dx/dt
// model_definition sees u b/c it's a global variable, it can't be an argument
void model_definition(const ublas_vector &x, ublas_vector &dxdt, const double &t)
{
  // Angle measurements in mrad
  dxdt[0] = x[1]/1000.;
  dxdt[1] = u[0]-9.81*sin(x[1]/1000.);
}

#endif
